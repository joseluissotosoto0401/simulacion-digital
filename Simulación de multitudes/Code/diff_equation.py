import numpy as np

'''
Clase para definir todos los parámetros físicos de la simulación. Funciones principales
integrar - para un solo paso - la dirección y la aceleración normalizadas (dadas
por la iteración entidad-entidad y pared-) para todos los agentes.
'''

class Diff_Equ:
    def __init__(self, num_individuals, L, tau, room, radii, weights):
        # condiciones iniciales
        self.room = room                            # impoertar la clase rom como room
        self.N = num_individuals                    # cantidad de entidades
        self.m = weights                            # masa de las entidades (kg)
        self.v_0 = 1.5 * np.ones(self.N)            # velocidad deseanda (m/s)
        self.radii = radii                          # radio de las entidades (m)
        self.A = 2*10**3                            # constante (N)
        self.B = 0.08                               # constante (m)
        self.tau = tau                              # time-step (s)
        self.k = 1.2*10**5                          # parametros (kg/s^2)
        self.kap = 2.4*10**5                        # parametros (kg/(m*s^2))
        self.L = L                                  # tamaño de la habitacion cuadrada(m)
        self.r_D = room.get_destination()           # posicion de la puerta (m,m)
        self.numwalls = self.room.get_num_walls()   # numero de paredes
        self.walls = self.room.walls                # posición de los puntos de esquina de las paredes
        self.wallshere = self.room.wallshere        # Cierto si hay paredes en el medio de la habitación

    #Comprueba si una entidad toca a otro o a una pared
    def g(self, x):        
        """devuelve un flotante, el valor del argumento si y solo si la entrada es negativa
             parametros:   ``x``: float
        """    
        if x < 0:
            return 0
        else:
            return x      
        
    #Suma los radios de las entidades i y j
    def rad(self, i, j):  
        """ devuelve un flotante, la suma de dos radios
            parameters: ``i``,``j``: integers (agent i and j)
        """
        return self.radii[i] + self.radii[j]

    #Distancia entre la entidad i con posicion r y muro j
    def wall_distance(self, i, j, r):
        """ returns:    ``distance``: float (distancia más pequeña entre la entidad i y la pared j)
                        ``n``, ``t``: 1D-array (vector normalizado apuntando desde
                                       muro a la entidad, y es vector tangencial)
                        ``nearest`` : 1D-array (punto en la pared más cercano a la entidad)
            paramters:  ``i``, ``j``: integers (entidad i and pared j)
                        ``r``       : 2D-array (posiciones de las entidades)
        """
        temp_wall      = self.walls[j,:,:]
        line_vec       = temp_wall[1,:]-temp_wall[0,:]
        pnt_vec        = r[:,i]-temp_wall[0,:]
        line_len       = np.linalg.norm(line_vec)
        line_unitvec   = line_vec/line_len
        pnt_vec_scaled = pnt_vec/line_len
        temp           = line_unitvec.dot(pnt_vec_scaled)    
        if temp < 0.0:
            temp = 0.0
        elif temp > 1.0:
            temp = 1.0
        nearest       = line_vec*temp
        dist          = pnt_vec-nearest
        nearest       = nearest + temp_wall[0,:]
        distance      = np.linalg.norm(dist)
        n             = dist/distance
        t             = np.array([-n[1], n[0]])
        return distance, n, t, nearest   
    
    #Distancia entre la entidad i y la entidad j
    def agent(self, i, j, r,v):
        """ returns:    ``d``      : float (dsitamcia entre entidades i y j)
                        ``n``,``t``: 1D-array (vector normalizado apuntando desde
                                      entidad i a j y su vector tangencial)
                        ``dv_t``   : float (diferencia de velocidad)
            parameters: ``i``,``j``: integers (entidad i ay j)
                        ``r``,``v``: 2D-array (posición y velocidad de todas las entidades)
        """
        d    = np.linalg.norm(r[:, i] - r[:, j])
        n    = (r[:, i] - r[:, j])/d
        t    = np.array([-n[1],n[0]])
        temp = v[:, j] - v[:, i]
        dv_t = temp.dot(t)
        return d, n, t, dv_t
    
    #Fuerza entre la entidad i y j
    def f_ij(self, i, j, r, v):  
        """ returns an 1D-array, the frocevector between agent i and j
            parameters: ``i``,``j``: integers (entidad i y j)
                        ``r``,``v``: 2D-array (posición y velocidad de todas las entidades)
        """
        d, n, t, dv_t = self.agent(i, j, r,v)
        rad_ij = self.rad(i,j)
        a = self.A * np.exp((rad_ij - d) / self.B) + self.k * self.g(rad_ij - d)
        b = self.kap * self.g(rad_ij - d) * dv_t
        return a * n + b * t
    
    #Fuerza entre la entidad i y la pared j
    def f_iW(self, i, j, r, v):  
        """ 
            retorna un 1D-array, el vector fuerza entre la entidad i y la pared j
            parameters: ``i``,``j``: integers (entidad i y pared j)
                        ``r``,``v``: 2D-array (posicion y velocidad de todos los agentes)
        """
        d,n,t = self.wall_distance(i, j, r)[:-1]
        a = self.A * np.exp((self.radii[i] - d) / self.B) + self.k * self.g(self.radii[i] - d)
        b = self.kap * self.g(self.radii[i] - d) * v[:, i].dot(t)
        return a * n - b * t

    #punto de intersección de dos rectas formadas por los puntos (a1,a2), respectivamente (b1,b2)
    def seg_intersect(self, a1,a2,b1,b2):
        """returns un 1D-array, la posición x,y del punto de intersección
            de lineas (a1,a2), (b1,b2)
           parameters: ``a1``,``a2``,``b1``,``b2``: 1D-array (xy-lines-points)
        """
        da = a2-a1
        db = b2-b1
        dp = a1-b1
        dap = np.array([-da[0][1],da[0][0]])
        denom = np.dot(dap, db)
        num = np.dot(dap, dp)
        return (num / denom.astype(float))*db + b1

    #Devuelve true si el punto c está entre los dos puntos a y b
    def is_between(self,a,b,c):
        """returns Verdadero si y solo si el punto c está entre los puntos a y b
           parameters:``a``,``b``,``c``: 1D-array (xy-points)
        """
        return np.linalg.norm(a-c) + np.linalg.norm(c-b) == np.linalg.norm(a-b) 
    
    #tomando la dirección correcta del vector de la entidad
    def direction(self,i,j,r):
        """devuelve una matriz 1D, la dirección del vector de la entidad i normalizada
           parameters: ``i``,``j``: integers (entidad i y pared j)
                       ``r``: 2D-array, (posición de todas entidadades)
        """
        wall = self.walls[j,:,:]
        wall_norm = (wall[0,:]-wall[1,:])/np.linalg.norm(wall[0,:]-wall[1,:])
        point = self.seg_intersect(r[:,i],self.r_D,wall[0,:],wall[1,:])
        t_or_f = self.is_between(wall[0,:],wall[1,:],point)
        
        if (t_or_f == 1 and np.linalg.norm(-r[:,i]+self.r_D)+self.radii[i] > np.linalg.norm(-point+self.r_D)) or (np.min([np.linalg.norm(r[:,i]-wall[0,:]),np.linalg.norm(r[:,i]-wall[1,:])]) < np.linalg.norm(2*self.radii[i]) and np.linalg.norm(-r[:,i]+self.r_D) > np.linalg.norm(-point+self.r_D)):
            
            e = self.nearest_path(wall,t_or_f,point,wall_norm,r[:,i],i)
        else:
            e = self.e_0(r[:,i],i)
        return e 

    #tomar la dirección correcta en caso de puntos de esquina de pared
    def e_1(self,r_i,temp_wall,i,j):
        """returns e un 1D-array, la dirección normalizada del vector i-entidad en caso
            de puntos de esquina en la habitación
           parameters: ``i``,``j``: integers (agent i and wall j)
                       ``r_i``: 1D-array, (entidad i y pared j)
                       ``temp_wall``: 1D-array, (posición de la pared j)
        """
        all_walls = self.walls[:,:,:]
        wall_norm = (temp_wall[0,:]-temp_wall[1,:])/np.linalg.norm(temp_wall[0,:]-temp_wall[1,:])
        check_close_corner = np.zeros((all_walls.shape[0],2))
        point = self.seg_intersect(r_i,self.r_D,temp_wall[0,:],temp_wall[1,:])
        t_or_f = self.is_between(temp_wall[0,:],temp_wall[1,:],point)
        
        for k in range(all_walls.shape[0]):
            if k == j:
                continue
            check_close_corner[k,0] = self.is_between(all_walls[k,0,:],all_walls[k,1,:],temp_wall[0,:])
            check_close_corner[k,1] = self.is_between(all_walls[k,0,:],all_walls[k,1,:],temp_wall[1,:])
        
        if np.sum(check_close_corner) == 1:
            for k in range(all_walls.shape[0]):
                if (check_close_corner[k,0] == 1 and check_close_corner[k,1] == 0) and self.is_between(all_walls[k,0,:],all_walls[k,1,:],self.seg_intersect(r_i,self.r_D,all_walls[k,0,:],all_walls[k,1,:])) == 1:
                    e = self.nearest_path(temp_wall,t_or_f,point,wall_norm,r_i,i)
                    break
                elif (check_close_corner[k,0] == 1 and check_close_corner[k,1] == 0) and self.is_between(all_walls[k,0,:],all_walls[k,1,:],self.seg_intersect(r_i,self.r_D,all_walls[k,0,:],all_walls[k,1,:])) == 0:
                    if np.linalg.norm(r_i-point)<self.radii[i]:
                        e = self.nearest_path(temp_wall,t_or_f,point,wall_norm,r_i,i)
                    elif (t_or_f == 1 and np.linalg.norm(point-r_i)<2*self.radii[i]):
                        e =  - wall_norm 
                        break
                    else:
                        p = temp_wall[1,:] - wall_norm*2*self.radii[i]
                        e = (-r_i+p)/np.linalg.norm(-r_i+p)
                        break
                elif check_close_corner[k,0] == 0 and check_close_corner[k,1] == 1 and self.is_between(all_walls[k,0,:],all_walls[k,1,:],self.seg_intersect(r_i,self.r_D,all_walls[k,0,:],all_walls[k,1,:])) == 1:
                    e = self.nearest_path(temp_wall,t_or_f,point,wall_norm,r_i,i)
                    break
                    
                elif check_close_corner[k,0] == 0 and check_close_corner[k,1] == 1 and self.is_between(all_walls[k,0,:],all_walls[k,1,:],self.seg_intersect(r_i,self.r_D,all_walls[k,0,:],all_walls[k,1,:])) == 0:    
                    if np.linalg.norm(r_i-point)<self.radii[i]:
                        e = self.nearest_path(temp_wall,t_or_f,point,wall_norm,r_i,i)
                    elif (t_or_f == 1 and np.linalg.norm(point-r_i)<2*self.radii[i]):
                        e =  wall_norm 
                        break
                    else: 
                        p = temp_wall[0,:] + wall_norm*2*self.radii[i]
                        e = (-r_i+p)/np.linalg.norm(-r_i+p)
                        break
        else:
            e = self.nearest_path(temp_wall,t_or_f,point,wall_norm,r_i,i)
        return e
    
    
    #tomar el camino más cercano si una entidad tiene que superar una pared
    def nearest_path(self,temp_wall,t_or_f,point,wall_norm,r_i,i):
        """returns an 1D-array, la dirección normalizada del vector i-entidad en caso
            de un muro entre la puerta y las entidades
           parameters: ``i``: integers (entidad i y muro j)
                       ``r_i``: 1D-array, (posicion de la entidad i)
                       ``temp_wall``: 1D-array, (posicion del muro j)
                       ``t_or_f: True or False, (comprobar si la puerta-entidad-línea se cruzan con la pared j)
                       ``point``: 1D-array, (punto de intersección entre el
                                   puerta-entidad-linea y temp_wall-line)
        """
        if (np.linalg.norm(self.r_D-temp_wall[0,:]) + np.linalg.norm(r_i-temp_wall[0,:])) <= (np.linalg.norm(self.r_D-temp_wall[1,:]) + np.linalg.norm(r_i-temp_wall[1,:])):
            #if (t_or_f == 1 and np.linalg.norm(point-r_i)<2*self.radii[i]):
            #    e =  wall_norm 
            #else: 
            p = temp_wall[0,:] + wall_norm*2*self.radii[i]
            e = (-r_i+p)/np.linalg.norm(-r_i+p)
        else:
            #if (t_or_f == 1 and np.linalg.norm(point-r_i)<2*self.radii[i]):
            #    e =  - wall_norm 
            #else:
            p = temp_wall[1,:] - wall_norm*2*self.radii[i]
            e = (-r_i+p)/np.linalg.norm(-r_i+p)
        return e
    

    #Dirección deseada normalizada para una entidad
    def e_0(self, r, i):  
        """ returns an 1D-array, the normalized desired direction of agent i
            parameters: ``i``: integer (entidad i)
                        ``r``: 1D-array (posicion de la entidad i)
        """
        #Si hay dos destinos, entonces la mitad de las personas van a cada destino.
        if len(self.r_D) == 2:
            if i < self.N/2:
                return (-r + self.r_D[0]) / np.linalg.norm(-r + self.r_D[0])
            else:
                return (-r + self.r_D[1]) / np.linalg.norm(-r + self.r_D[1])

        return (-r + self.r_D) / np.linalg.norm(-r + self.r_D)

    #Encontrar la pared más cercana que obstruye a una persona
    def nearest_wall(self,r_i):
        """retrns an integer, el argumento del respeto de la pared interna más cercana 
           to the agent i
           parameters: ``r_i``: 1D-array, (posicion de la entidad i)
        
        """
        all_walls = self.walls[5:,:,:]
        distance = np.zeros((all_walls.shape[0]))
        for i in range(all_walls.shape[0]):
            temp_wall = all_walls[i,:,:]
            point = self.seg_intersect(r_i,self.r_D,temp_wall[0,:],temp_wall[1,:])
            distance[i] = np.linalg.norm(point-r_i)
        return 5+np.argmin(distance)
  

    #Dirección deseada normalizada para todas las entidades
    def e_t(self, r):
        """ returns an 1D-array, la dirección deseada normalizada en la dirección real
             paso de tiempo para todas las entidades
            parameters: ``r``: 2D-array (posicione de todas las entidades)
        """
        e_temp = np.zeros((2, self.N))
        #Si hay paredes adicionales la dirección deseada no tiene que ser la dirección de una puerta.
        if self.wallshere == False: 
            for i in range(self.N):
                e_temp[:, i] = self.e_0(r[:,i], i)
        else:
            for i in range(self.N):
                j = self.nearest_wall(r[:,i])
                e_temp[:, i] = self.direction(i,j,r)
        return e_temp


    #The interacting force of the agents to each other  
    def f_ag(self, r, v):
        """ returns a 3D-array, las fuerzas que interactúan entre todas las entidades
            parameters: ``r``,``v``: 2D-array (posición y velocidad de todas las entidades) 
        """
        f_agent = np.zeros((2, self.N))
        fij     = np.zeros(((2, self.N, self.N)))
        for i in range(self.N-1):
            for j in range(self.N-1-i):
                    fij[:,i,j+i+1] = self.f_ij(i,j+i+1,r,v)
                    fij[:,j+i+1,i] = -fij[:,i,j+i+1]
        f_agent = np.sum(fij, 2)
        return f_agent

    #La fuerza de cada muro que actúa sobre cada entidad.
    def f_wa(self, r, v):
        """ returns a 3D-array, las fuerzas que interactúan entre todas las entidades y paredes
            parameters: ``r``,``v``: 2D-array (posición y velocidad de todas las entidades) 
        """
        f_wall = np.zeros((2, self.N))
        for i in range(self.N):
            for j in range(self.numwalls):
                f_wall[:, i] += self.f_iW(i, j, r, v)
        return f_wall
    
#La ecuación diferencial de nuestro problema 
#Calcula la aceleración de cada agente
    def f(self, r, v):
        """ returns a 2D-array 
        v = la velocidad en el tiempo t
        r = la posición en el tiempo t"""
        e_temp = self.e_t(r)
        acc = (self.v_0 * e_temp - v) / self.tau + self.f_ag(r, v) / self.m + self.f_wa(r, v) / self.m
        return acc

