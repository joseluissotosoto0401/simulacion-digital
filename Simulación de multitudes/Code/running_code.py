from Simulation_class import Simulation



sim = Simulation(num_individuals=20, num_steps=500, method="leap_frog", room_size=25, room="square")
sim.fill_room()                        
sim.run()                             
sim.show(wait_time=50, sim_size=500)   
